
class Board

attr_reader :grid

  def initialize(size)
    @grid = Array.new(size) { Array.new(size) }
  end

  def [](row, col)
    @grid[row][col]
  end

  def []=(row, col, shot)
    @grid[row][col] = shot
  end

  def show_board(ships = 5)
    positions = []
    until positions.count == ships
      position = [rand(@grid.count), rand(@grid.count)]
      self[*position] = :ship unless positions.include?([*position])
      positions << position unless positions.include?([*position])
    end
  end

  def count
    @grid.reduce(0) { |add, row| add += row.count(:ship) }
  end

  def show
    show = { nil => "-", miss: "/", ship: "-", hit: "$" }
    grid_show = @grid.map do |row|
      row.map { |el| show[el] }
    end
    grid_show.map! { |line| line.join(" | ")}
    grid_show.join("\n#{">>>>" * (@grid.count - 1)}>>\n")
  end

  def in_range?(position)
    position.all? { |el| (0...@grid.count).to_a.include?(el) }
  end

end
