
class HumanPlayer
  def initialize(size)
    @size = size
  end

  def ask
    puts "Choose a spot to fire on x axis!"
    x_axis = gets.to_i
    puts "Choose a spot to fire on y axis!"
    y_axis = gets.to_i
    [y_axis, x_axis]
  end

end
