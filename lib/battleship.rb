require_relative 'board'
require_relative 'player'

class BattleshipGame

  attr_reader :board

  def initialize(size = 5)
    @board = Board.new(size = 5)
  end

  def play(ships)
    system('clear')
    @board.show_board(ships = 5)
    @player = HumanPlayer.new(@board.grid.count)
    until @board.count == 0
      play_turn
    end
    finish
  end

  def play_turn
    show_board
    position = @player.ask
    unless board.in_range?(position)
      no_space_there
      return
    end
    fire(position)
  end

  def fire(position)
    not_valid if board[*position] == :miss || board[*position] == :hit
    good_shot(position) if board[*position] == :ship
    bad_shot(position) if board[*position] == nil
  end

  def good_shot(position)
    system('clear')
    board[*position] = :hit
    puts "Nice shot, you hit a ship!"

  end

  def bad_shot(position)
    system('clear')
    board[*position] = :miss
    puts "Bad shot, you just missed it!"

  end

  def show_board
    puts "#{@board.show}"
  end

  def not_valid
    system('clear')
    puts "Don't waste your ammo, you've already fired there!"

  end

  def no_space_there
    system('clear')
    puts "You can't shoot there!"
  end

  def finish
    system('clear')
    puts "#{@board.show}"
    puts "NICE SHOTS! YOU SUNK ALL MY SHIPS! I'LL SINK YOU NEXT TIME!"
  end
end



system('clear')
game = BattleshipGame.new
game.play(5)
